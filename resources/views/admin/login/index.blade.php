<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- bootstrap 3.0.2 -->
        {!!Html::style('intranet/css/bootstrap.min.css')!!}
        <!-- font Awesome -->
        {!!Html::style('intranet/css/font-awesome.min.css')!!}
        <!-- Ionicons -->
        {!!Html::style('intranet/css/ionicons.min.css')!!}
        <!-- Morris chart -->
        {!!Html::style('intranet/css/morris/morris.css')!!}
        <!-- jvectormap -->
        {!!Html::style('intranet/css/jvectormap/jquery-jvectormap-1.2.2.css')!!}
        <!-- fullCalendar -->
        {!!Html::style('intranet/css/fullcalendar/fullcalendar.css')!!}
        <!-- Daterange picker -->
        {!!Html::style('intranet/css/daterangepicker/daterangepicker-bs3.css')!!}
        <!-- bootstrap wysihtml5 - text editor -->
        {!!Html::style('intranet/css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')!!}
        <!-- Theme style -->
        {!!Html::style('intranet/css/AdminLTE.css')!!}

        <!-- APLICA EL ESTILO AL SELECT -->
        {!!Html::style('intranet/css/select/css/bootstrap-select.css')!!}

        {!!Html::style('intranet/css/producto/css/bootstrap-selectpro.css')!!}

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>

    
    <body class="skin-blue">
        <!-- header logo: style can be found in header.less -->
            <div class="form-box" id="login-box">
             <div class="header" >Test
                    <!-- <li class="user-header bg-light-blue">

                      <div align="center">  <img src="img/logo_corporacion.png" class="" alt="User Image"  width="70" height="70"/></div>         
                    </li>--> 
             </div>

            <form >
                 <!-- form action="../../index.html" method="post" -->


                <input type="hidden" name="_token" value="{{csrf_token()}}" id="token">

               
               
                <div class="body bg-gray">
                    <div class="form-group">
                        <input id="tx_usuario_login" type="text" autofocus="" name="userid" class="form-control"  placeholder="Usuario"/>
                    </div>
                    <div class="form-group">
                        <input id="tx_clave_login" type="password" name="password" class="form-control" placeholder="Clave"/>
                    </div>
                   
                    <div class = "captcha"> 

                            <span> {!!captcha_img() !!} </span> 
                            <button type = "button" id = "refrescar_captcha" class = "btn btn-success btn-refresh"> <i class = "fa fa-refresh"> </i> </button>   
                    </div> 

                    <H4><b> <label>No Entiende La Imagen? Haga Click en Refrescar </label> </b></H4>

                    <input id = "captcha" type = "text" class = "form-control" placeholder = "Introduzca Codigo Imagen" name = "captcha"> 

                    <p align="left-side" class="errorcaptcha alert alert-danger"  style="width:300px;height:40px;display:none"></p>

                    <input type="hidden" value="" id="txtoculto_latitud_login">
                    <input type="hidden" value="" id="txtoculto_longitud_login">
                   
                   @include('admin.login.validacion-modal')
                </div>
                <div class="footer">                                                               
                    <button  id="btnmodalcerrar_login" type="button"   class=" btn btn-primary  btn-block">Aceptar</button>  
                </div>

            </form>

           
        </div>

        <!-- add new calendar event modal -->


        <!-- jQuery 2.0.2 -->
        <!-- jQuery 2.0.2 -->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>


        {!!Html::script('intranet/js/ajax/login/operaciones.js')!!}

         {!!Html::script('intranet/js/ajax/operacionescomunes/operaciones.js')!!}
        
        <!-- jQuery UI 1.10.3 -->
        {!!Html::script('intranet/js/jquery-ui-1.10.3.min.js')!!}
        <!-- Bootstrap -->
        {!!Html::script('intranet/js/bootstrap.min.js')!!}
        <!-- Morris.js charts
        <script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script> -->
        <!-- Sparkline -->
        {!!Html::script('intranet/js/plugins/sparkline/jquery.sparkline.min.js')!!}
        <!-- jvectormap -->
        {!!Html::script('intranet/js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')!!}
        {!!Html::script('intranet/js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')!!}
        <!-- fullCalendar -->
        {!!Html::script('intranet/js/plugins/fullcalendar/fullcalendar.min.js')!!}
        <!-- jQuery Knob Chart -->
        {!!Html::script('intranet/js/plugins/jqueryKnob/jquery.knob.js')!!}
        <!-- daterangepicker -->
        {!!Html::script('intranet/js/plugins/daterangepicker/daterangepicker.js')!!}
        <!-- Bootstrap WYSIHTML5 -->
        {!!Html::script('intranet/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')!!}
        <!-- iCheck -->
        {!!Html::script('intranet/js/plugins/iCheck/icheck.min.js')!!}
        <!-- AdminLTE App -->
        {!!Html::script('intranet/js/AdminLTE/app.js')!!}
        <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
        {!!Html::script('intranet/js/AdminLTE/dashboard.js')!!}

          <!-- APLICA EL ESTILO AL SELECT -->
        {!!Html::script('intranet/css/select/js/bootstrap-select.js')!!}

        {!!Html::script('intranet/css/producto/js/bootstrap-selectpro.js')!!}

        

         
        @section('scripts')

             <script type="text/javascript">
                    obtenerubicacionacceso();
             </script>

        @show    
    </body>
</html>