<!DOCTYPE html>
<html>
    <head><meta http-equiv="Content-Type" content="text/html; charset=gb18030">

        
        <title>Test</title>

        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- bootstrap 3.0.2 -->
        {!!Html::style('intranet/css/bootstrap.min.css')!!}
        <!-- font Awesome -->
       <!-- {!!Html::style('intranet/css/font-awesome.min.css')!!} -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <!-- Ionicons -->
        {!!Html::style('intranet/css/ionicons.min.css')!!}
        
        {!!Html::style('intranet/css/AdminLTE.css')!!}

    </head>
    <body class="skin-blue">
        <!-- header logo: style can be found in header.less -->
        <header class="header">
            <a href="/" class="">
                <!-- Add the class icon to your logo image or logo icon to add the margining -->
                     <!--<p align="right-side">_________________________________________________________</p>-->
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->

                <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    
                </a>
                <div class="navbar-left">
                    <ul class="nav navbar-nav">
                        <!-- User Account: style can be found in dropdown.less -->
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class=""></i>
                                <span>Test<i class=""></i></span>
                            </a>
                        </li>
                    </ul>
                </div>

                <div class="navbar-right">
                    <ul class="nav navbar-nav">
                        <!-- User Account: style can be found in dropdown.less -->
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="glyphicon glyphicon-user"></i>

                                <span>Usuario <i class="caret"></i></span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- User image -->
                                <li class="user-header bg-light-blue">
 
                                        <?php

                                            if($fotopersona!=""){

                                                echo '<img src="data:image/jpeg;base64,'.base64_encode($fotopersona).'"  class="img-circle" alt="User Image"/> ';
                                            }
                                            else{

                                                echo '<img src="img/fotoblanco.png"  class="img-circle" alt="User Image"/> ';

                                            }
                                        ?>
                                   
                                    <p>
                                        Usuario -  {!!ucwords(Auth::user()->co_usuario)!!} {{$personaaccesa}}
                                        <small></small>
                                    </p>
                                </li>
                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    <div class="">
                                        <a  href="{!!URL::to('/logout')!!}" class="btn btn-primary  btn-flat">Cerrar Sesion</a> 

                            
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="left-side sidebar-offcanvas">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    
                        <div class="user-panel">
                        <div class="pull-left image">

                        <?php

                            if($fotopersona!=""){

                                echo '<img src="data:image/jpeg;base64,'.base64_encode($fotopersona).'"  class="img-circle" alt="User Image"/> ';
                            }
                            else{

                                echo '<img src="img/fotoblanco.png"  class="img-circle" alt="User Image"/> ';
                            }

                        ?>

                        </div>
                        <div class="pull-left info">
                            <p>Bienvenido, {!!ucwords(Auth::user()->co_usuario)!!} {{$personaaccesa}}
                            </p>
                            
                            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                        </div>
                    </div>
                    <!-- search form -->
                    <!-- /.search form -->
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    
                            
                    <ul class="sidebar-menu">
                        
                        <li class="treeview">


                            <li>
                                <a href="{!!URL::to('restaurantes')!!}"><i class="fa fa-tasks"></i>Menu 1</a>
                            </li>

                            <li>
                                <a href="{!!URL::to('restauranteslistar')!!}"><i class="fa fa-tasks"></i>Menu 2</a>
                            </li>

                            <li>
                                <a href="{!!URL::to('reservar')!!}"><i class="fa fa-tasks"></i>Menu 3</a>
                            </li>

                            <li>
                                <a href="{!!URL::to('reservaslistar')!!}"><i class="fa fa-tasks"></i>Menu 4</a>
                            </li>

                        </li>   

                    </ul>    

                    
                       
             </section>
                <!-- /.sidebar -->
            </aside>

            <!-- Right side column. Contains the navbar and content of the page -->
            
           <div id="principalPanel">
                <aside class="right-side">
                    @yield('content') 
                </aside><!-- /.right-side -->
           </div>

        </div><!-- ./wrapper -->

        <!-- add new calendar event modal -->


        <!-- jQuery 2.0.2 -->
        <!-- jQuery 2.0.2 -->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>

        
        <!-- jQuery UI 1.10.3 -->
        {!!Html::script('intranet/js/jquery-ui-1.10.3.min.js')!!}
        <!-- Bootstrap -->
        {!!Html::script('intranet/js/bootstrap.min.js')!!}
        
        {!!Html::script('intranet/js/plugins/iCheck/icheck.min.js')!!}
        <!-- AdminLTE App -->
        {!!Html::script('intranet/js/AdminLTE/app.js')!!}
        <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
        {!!Html::script('intranet/js/AdminLTE/dashboard.js')!!}
         
         @section('scripts')

           
                {!!Html::script('intranet/js/ajax/operacionescomunes/operaciones.js')!!}

              


        <script type="text/javascript">


                 
               

        </script>
        
        
        

        @show    
    </body>
</html>