<?php

/*
|--------------------------------------------------------------------------

| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {
    return view('welcome');
});

Route::get('/', 'FrontController@index');

Route::group(['middleware' => 'auth'], function () {
    Route::get('admin', 'FrontController@admin');

//RUTAS LOGIN
    Route::post('iniciar_sesion/', 'Auth\AuthController@iniciar_sesion');
    Route::get('buscar_usuario/{co_usuario}', 'LogController@buscar_usuario');

    Route::get('estatus_usuario/{co_usuario}', 'LogController@estatus_usuario');
    Route::get('logout', 'LogController@logout');

    
//FIN RUTAS LOGIN


//RUTAS CAPTCHA
    Route::get('refrescarcaptcha', 'CaptchaController@RefrescarCaptcha');
    Route::post('validarcaptcha', 'CaptchaController@ValidarCaptcha');
//


//ADMINISTRAR RESTAURANT

    Route::get('restaurantes/', 'AdministrarRestaurantController@Admirestaurant');

    Route::post('listaradmirestaurant/', 'AdministrarRestaurantController@Listaradmirestaurant');

    Route::post('ingresarrestaurant/', 'AdministrarRestaurantController@IngresarAdmirestaurant');

    Route::get('obtenerrestaurant/{id}', 'AdministrarRestaurantController@ObtenerRestaurant');

    Route::post('actualizarrestaurant/', 'AdministrarRestaurantController@ActualizarRestaurant');

    Route::post('obtenerasociacionrestaurant/{id}', 'AdministrarRestaurantController@AsosiacionRestaurant');

    Route::get('eliminarrestaurant/{id}', 'AdministrarRestaurantController@EliminarRestaurant');

//FIN ADMINISTRAR RESTAURANT

//LISTAR RESTAURANT

    Route::get('restauranteslistar/', 'ListarRestaurantController@Restauranteslistar');

    Route::post('listarrestaurant/', 'ListarRestaurantController@Listarrestaurant');

//FIN LISTAR RESTAURANT

//RESERVAS

    Route::get('reservar/', 'ReservarController@Reservar');

    Route::post('listarreservar/', 'ReservarController@ListarReservar');

    Route::get('cargarrestaurante/', 'ReservarController@CargarRestaurante');

    Route::post('ingresarreserva/', 'ReservarController@IngresarReserva');

    Route::get('buscarcantidadreservas/{idrestaurant}/{fecha}', 'ReservarController@BuscarCantidadReservas');

    Route::get('recargarrestaurant/{criterio}', 'ReservarController@RecargarRestaurantes');
   

//FIN RESERVAS

//LISTAR RESERVAS

    Route::get('reservaslistar/', 'ListarReservarController@Reservaslistar');

    Route::post('listarreservas/', 'ListarReservarController@Listarreservas');


//FIN LISTAR RESERVAS



});
