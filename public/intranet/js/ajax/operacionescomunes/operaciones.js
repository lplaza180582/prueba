//VARIABLES GLOBALES

data_registros = [];
data_registrosfiltrados = [];

//////////////////////////////////////


function validarsololetrasnumeros(evt) {

    //alert("pase por aqui solo numeros y letras");

    evt = (evt) ? evt : event

    fnPreventBackspacePropagation(evt);
    var key = (evt.which) ? evt.which : evt.keyCode;

    //alert(key);
    //rango: 47-58 (numeros)
    //rango: 64-91 (letras mayusculas)
    //rango: 96-123 (letras minisculas)
    //a tecla 8 borrar

    if ((key > 47 && key < 58) || (key > 64 && key < 91) || (key > 96 && key < 123) || key == 8) {
        return true;
    } else {
        return false;
    }
}


// FUNCION QUE HABILITA LA TECLA BACKSPACE
function fnPreventBackspacePropagation(event) {
    var BACKSPACE_NAV_DISABLED = true;
    //alert("pase por aqui segunda funcion");

    if (BACKSPACE_NAV_DISABLED && event.keyCode == 8)


    {
        //alert("pase por aqui segunda funcion");
        event.stopPropagation();
    }
    return true;
}



function validarnumero(evt, identificador) {
    //alert(evt);
    evt = (evt) ? evt : event

    //$(document).keydown(function(e) { if (evt.keyCode == 8) e.stopPropagation(); });

    //alert("pase por aqui");
    fnPreventBackspacePropagation(evt);



    var key = (evt.which) ? evt.which : evt.keyCode;
    if (key > 47 && key < 58 || key == 8 || key == 37 || key == 39 || key == 9) {

        var valuecantidad = "#cantidad" + identificador;
        var valor_anterior = $(valuecantidad).val();

        var valorpresionado = String.fromCharCode(key);

    } else {

        var valorpresionado = String.fromCharCode(key);

        return false;
    }
}

function validarnumerodecimal(evt) {

    //alert("pase por aqui numero decimal");
    //alert(evt);
    evt = (evt) ? evt : event

    fnPreventBackspacePropagation(evt);

    var key = (evt.which) ? evt.which : evt.keyCode;
    if (key > 47 && key < 58 || key == 8 || key == 46) {
        return true;
    } else {
        return false;
    }
}

function validarteclaenter(evt) {

    //alert("pase por aqui numero decimal");
    //alert(evt);
    evt = (evt) ? evt : event
    var key = (evt.which) ? evt.which : evt.keyCode;

    //alert(key);
    if (evt.which == 13) {

        //alert(key);
        return false;
    }

}



function validarteclaenterconvertirmayusminusletrasnumeros(e, solicitar) {


    e = (e) ? e : e

    fnPreventBackspacePropagation(e);


    var key = (e.which) ? e.which : e.keyCode;

    //alert(key);
    if (e.which == 13) {

        //alert(key);
        return false;
    } else {
        //alert("pase por aqui");
        // Admitir solo letras
        tecla = (document.all) ? e.keyCode : e.which;
        //alert(tecla);
        //if ((tecla > 47 && tecla < 58) || (tecla > 64 && tecla < 91) || (tecla > 96 && tecla < 123) || tecla == 8) return true;

        if (tecla == 8) return true;

        patron = /[\D\s]/;
        patron2 = /[\D\0-9]/;
        patron3 = /[\D\s\0-9]/;
        //alert("patron"+patron);

        te = String.fromCharCode(tecla);
        //alert("te"+te);

        if ((!patron.test(te)) && (!patron2.test(te)) && (!patron3.test(te))) return false;
        // No amitir espacios iniciales y convertir 1ª letra a mayúscula
        txt = solicitar.value;
        if (txt.length == 0 && te == ' ') return false;
        if (txt.length == 0 || txt.substr(txt.length - 1, 1) == ' ') {
            solicitar.value = txt + te.toUpperCase();
            return false;
        }

    }

}


function validarteclaenterconvertirmayusminussololetras(e, solicitar) {


    e = (e) ? e : e

    fnPreventBackspacePropagation(e);
    var key = (e.which) ? e.which : e.keyCode;

    //alert(key);
    if (e.which == 13) {

        //alert(key);
        return false;
    } else {
        //alert("pase por aqui");
        // Admitir solo letras
        tecla = (document.all) ? e.keyCode : e.which;
        //alert(tecla);
        //if ((tecla > 47 && tecla < 58) || (tecla > 64 && tecla < 91) || (tecla > 96 && tecla < 123) || tecla == 8) return true;

        if (tecla == 8) return true;

        patron = /[\D\s]/;
        //alert("patron"+patron);

        te = String.fromCharCode(tecla);
        //alert("te"+te);

        if (!patron.test(te)) return false;
        // No amitir espacios iniciales y convertir 1ª letra a mayúscula
        txt = solicitar.value;
        if (txt.length == 0 && te == ' ') return false;
        if (txt.length == 0 || txt.substr(txt.length - 1, 1) == ' ') {
            solicitar.value = txt + te.toUpperCase();
            return false;
        }

    }

}

function validar_fechasmesactual(fechaenviada, fechaactual) {

    var x = fechaenviada.split("-");
    var z = fechaactual.split("-");

    //Cambiamos el orden al formato americano, de esto dd/mm/yyyy a esto mm/dd/yyyy
    fechadesdecomparacion = x[0] + "-" + x[1] + "-" + x[2];
    fechaactualcomparacion = z[0] + "-" + z[1] + "-" + z[2];


    var mesactual= z[1];
    var añoactual= z[0];

    var mesfechaenviada= x[1];
    var añofechaenviada= x[0];


    //alert("fechaactual"+mesactual+añoactual);
    //alert("fechaenviada"+mesfechaenviada+añofechaenviada);
    //Comparamos las fechas
    if (mesfechaenviada > mesactual) {

        //alert("es mayor mes");
        return 0;
    } 


    if (añofechaenviada > añoactual) {

        //alert("es mayor año");
        return 0;
    } 

    if (mesfechaenviada < mesactual) {

        //alert("es mayor mes");
        return 0;
    } 


    if (añofechaenviada < añoactual) {

        //alert("es mayor año");
        return 0;
    } 


}

function validar_fechasdiaactual(fechaenviada, fechaactual) {

    //alert("pase por aqui");
    //alert(fechaactual);

    var x = fechaenviada.split("-");
    var z = fechaactual.split("-");


    //Cambiamos el orden al formato americano, de esto dd/mm/yyyy a esto mm/dd/yyyy
    fechadesdecomparacion = x[0] + "-" + x[1] + "-" + x[2];
    fechaactualcomparacion = z[0] + "-" + z[1] + "-" + z[2];

    var diaactual= z[2];
    var mesactual= z[1];
    var añoactual= z[0];

    var diafechaenviada= x[2];
    var mesfechaenviada= x[1];
    var añofechaenviada= x[0];

    var largodiaactual= diaactual.length;

    //SE HACE DE ESTA MANERA PORQUE EN LA FECHA ACTUAL SE AGREGA UN 0
    //SI LOS DIAS TIENEN 2 DIGITOS Y HAY QUE EXTRAER ESE 0
    //ES DECIR EN VEZ DE 27 ENVIA 027
    if(largodiaactual==3){
      var diaactual = diaactual.substring(1,3);
    }


    //Comparamos las fechas
    if ((mesfechaenviada == mesactual)&&(añofechaenviada == añoactual)) {

        //alert(diaactual+diafechaenviada);

        if(diafechaenviada > diaactual){

            //alert("la fecha es mayor a la fecha actual");
            return 0;

        }

    } 
 

}



function validar_fechas(fechaInicial, fechaFinal) {

    var x = fechaInicial.split("-");
    var z = fechaFinal.split("-");

    //Cambiamos el orden al formato americano, de esto dd/mm/yyyy a esto mm/dd/yyyy
    fecha1 = x[0] + "-" + x[1] + "-" + x[2];
    fecha2 = z[0] + "-" + z[1] + "-" + z[2];

    //alert("fecha1"+fecha1);
    //alert("fecha2"+fecha2);
    //Comparamos las fechas
    if (Date.parse(fecha1) > Date.parse(fecha2)) {
        //alert("es mayor");
        return 0;
    } else {
        //alert("es menor");
        return 1;
    }

}

function validar_correo(correo) {
    
    var regex = /[\w-\.]{2,}@([\w-]{2,}\.)*([\w-]{2,}\.)[\w-]{2,4}/;

    if (regex.test(correo.trim())) {
       return true;

    } else {
       return false;
    }
}

function obtener_fechaactual() {

    var hoy = new Date();
    var dd = hoy.getDate();
    var mm = hoy.getMonth() + 1; //hoy es 0!
    var yyyy = hoy.getFullYear();

    if (dd < 10) {
        dd = '0' + dd
    }

    if (mm < 10) {
        mm = '0' + mm
    }

    fechahoy = yyyy + '-' + mm + '-' + dd;

    return fechahoy;

}


function activarCargando() {
    //alert("pase por aqui");
    var cargando = $("#load");
    var procesandoinfo = $("#procesainfo");
    procesandoinfo.show();
    cargando.button('loading');
    cargando.show();

}

function desactivarCargando() {
    $("#load").hide();
    $("#procesainfo").hide();
}

function activarCargandoed() {
    //alert("pase por aqui");
    var cargando = $("#loaded");
    var procesandoinfo = $("#procesainfoed");
    procesandoinfo.show();
    cargando.button('loading');
    cargando.show();

}

function desactivarCargandoed() {
    $("#loaded").hide();
    $("#procesainfoed").hide();
}

function activarCargandoel() {
    //alert("pase por aqui");
    var cargando = $("#loadel");
    var procesandoinfo = $("#procesainfoel");
    procesandoinfo.show();
    cargando.button('loading');
    cargando.show();

}

function desactivarCargandoel() {
    $("#loadel").hide();
    $("#procesainfoel").hide();
}

function activarCargandorevi() {
    //alert("pase por aqui");
    var cargando = $("#loadrevi");
    var procesandoinfo = $("#procesainforevi");
    procesandoinfo.show();
    cargando.button('loading');
    cargando.show();

}

function desactivarCargandorevi() {
    $("#loadrevi").hide();
    $("#procesainforevi").hide();
}

function filtrar_data(data, term, registros) {

    data_registros = [];
    data_registrosfiltrados = [];

    var contiene;
    if ($(data).filter(function(index) {
            contiene = this.text.toLowerCase().indexOf(term.toLowerCase()) != -1;
            if (contiene) {

                data_registrosfiltrados.push(data[index]);
                //console.log(data_registrosfiltrados);
                return data_registrosfiltrados;
            }

        }).length > 0) {


    } else {

        return data_registros;
    }
}

function asuntocorreo(descripconcepto){

   var asunto= "Solicitud Creada "+descripconcepto;

   return(asunto);
}

function asuntocorreoeditada(descripconcepto){

   var asunto= "Solicitud Editada "+descripconcepto;

   return(asunto);
}

function asuntocorreoeliminada(descripconcepto){

   var asunto= "Solicitud Eliminada "+descripconcepto;

   return(asunto);
}

function textonumerosolicitud(){

  var mensaje="Se ha Generado Satisfactoriamente la Solicitud con el Numero: ";  
  return(mensaje);
}

function textonumerosolicitudeditada(){

  var mensaje="Se ha Editado Satisfactoriamente la Solicitud con el Numero: ";  
  return(mensaje);
}

function textonumerosolicitudeliminada(){

  var mensaje="Se ha Eliminado Satisfactoriamente la Solicitud con el Numero: ";  
  return(mensaje);
}

function textodescripcion(){

  var mensaje=" Con la Descripcion: ";
  return(mensaje);
}

function textoelaboradapor(){

   var mensaje= " Elaborada por: ";
   return(mensaje);
}

function textoaprobadarechazada(){

   var mensaje="Esta Solictud Debe ser Aprobada o Rechaza por Usted.";
   return(mensaje);
}

function asuntocorreoaprobar(descripconcepto){

   var asunto= "Aprobada Solicitud "+descripconcepto;

   return(asunto);
}

function textonumerosolicitudaprobar(){

  var mensaje="La Solicitud con el Numero: ";  
  return(mensaje);
}

function textoaprobadopor(){

   var mensaje= " Ha sido Aprobada por: ";
   return(mensaje);
}

function textoobservacion(){

   var mensaje= " Con la siguiente Observacion: ";
   return(mensaje);
}

function asuntocorreorechazar(descripconcepto){

   var asunto= "Rechazada Solicitud "+descripconcepto;

   return(asunto);
}

function textonumerosolicitudrechazar(){

  var mensaje="La Solicitud con el Numero: ";  
  return(mensaje);
}

function textorechazadopor(){

   var mensaje= " Ha sido Rechazada por: ";
   return(mensaje);
}

function asuntocorreorevisar(descripconcepto){

   var asunto= "Revisada Solicitud "+descripconcepto;

   return(asunto);
}

function textonumerosolicitudrevisar(){

  var mensaje="La Solicitud con el Numero: ";  
  return(mensaje);
}

function textorevisadopor(){

   var mensaje= " Ha sido Revisada por: ";
   return(mensaje);
}

function notarevisado(){

   var mensaje= " Nota: Para que la solicitud sea Aprobada es necesario el Numero de Documento ";
   return(mensaje);
}

function darformatofecha(fecha){

    var yyyy=fecha.substr(0,4);
    var mm=fecha.substr(4,2);
    var dd=fecha.substr(6,2);

    fechaformateada = yyyy + '-' + mm + '-' + dd;

    //alert(fechaformateada);
 
    return(fechaformateada);
}

function diastrancurridosentrefechas(fechaInicial, fechaFinal) {

    var diferencia =  Math.floor(( Date.parse(fechaFinal) - Date.parse(fechaInicial) ) / 86400000);

    if(diferencia < 0){
           diferencia = diferencia*(-1);
    }
    //alert(diferencia);
    return(diferencia)

}

function obtenerubicacionacceso() {
    //alert("pase por aqui");

    if (navigator.geolocation) {
            var tiempo_de_espera = 3000;
            navigator.geolocation.getCurrentPosition(mostrarCoordenadas, mostrarError, { enableHighAccuracy: true, timeout: tiempo_de_espera, maximumAge: 0 } );
    }
    else {
            alert("La Geolocalización no es soportada por este navegador");
    }
}

function mostrarCoordenadas(position) {

    $("#txtoculto_latitud_login").val(position.coords.latitude);
    $("#txtoculto_longitud_login").val(position.coords.longitude);
    //alert("Latitud: " + position.coords.latitude + ", Longitud: " + position.coords.longitude);
}

function mostrarError(error) {
    var errores = {1: 'Permiso denegado', 2: 'Posición no disponible', 3: 'Expiró el tiempo de respuesta'};
    //alert("Error: " + errores[error.code]);
}













                   

                    





