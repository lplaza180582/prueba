<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\CaptchaRequest;

use Illuminate\Support\Facades\Input; 

use Validator;


class CaptchaController extends Controller
{

    public function ValidarCaptcha(Request $request)
    {

       $captcha=$request['captcha']; 

        if($captcha==null){

            //dd("pase por aqui".$request['captcha']);
            //die();
            return response()->json([
            "msj" => "captchaenblanco"
           ]);

        }
        else{

            $rules =  array('captcha' => ['required', 'captcha']); 
            $validator = Validator::make( 
                [ 'captcha' => Input::get('captcha') ], 
                $rules, 

                // Mensaje de error personalizado 
            [ 'captcha' => 'El captcha ingresado es incorrecto.' ]
            ); 


            //dd($validator->passes());
            //die();

            if ($validator->passes()) { 
                return response()->json([
                "msj" => "captchacorrecto"
                ]);
            } 
            else { 
                return response()->json([
                "msj" => "captchaincorrecto"
                ]);
       
            } 

        }

    }

    public function RefrescarCaptcha()
    {
        return response()->json(['captcha'=> captcha_img()]);
    }
}
