<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Session;
use Redirect;
use DB;

class LogController extends Controller
{
    

    //******************************************************************************//
    //ESTA FUNCION SE LLAMA EN EL MOMENTO QUE EL USUARIO CIERRA LA SESSION
    //******************************************************************************//

    public function logout(){
        
        Auth::logout();
       
        return Redirect::to('/');
        
        //return redirect('/');
    }


    public function buscar_usuario(Request $request, $co_usuario)
    {


        if ($request->ajax()) {

            //dd($co_usuario);
            //die();


            $usuariopersonal = DB::table('users')
                    ->select(
                        'users.co_usuario'
                    )    
            ->where('users.co_usuario',$co_usuario)
            ->count();

            //dd($usuariopersonal);
            //die();


            if($usuariopersonal >0){


                $usuarioestatus = DB::table('users')
                        ->select(
                             'users.activo'
                             
                        )
                        
                         ->where('users.co_usuario',$co_usuario)
                         ->get();

                    //dd($usuarioestatus);
                    //die();
             }
             else{

                 $usuarioestatus="NO EXISTE USUARIO";

             }


            return response()->json($usuarioestatus);


        }

    }

    
     
    public function estatus_usuario(Request $request, $co_usuario)
    {


        if ($request->ajax()) {

            $usuario = DB::table('users')
                ->select(
                  
                    'users.st_usuario'
                )
                ->where('users.co_usuario', $co_usuario)
                ->get();


            return response()->json($usuario);


        }

    }


}



