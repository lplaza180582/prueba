<?php


namespace App\Http\Requests;

//use App\Http\Requests\Request;
use Illuminate\Foundation\Http\FormRequest;


//namespace App\Http\Requests;

//use Illuminate\Foundation\Http\FormRequest;

class CaptchaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array



     */
    public function rules()
    {

        //echo "pase por aqui";
        return [
          'captcha' => 'required|captcha',
        ];

    }

     public function messages() {
        return [
        "captcha.required" => "Debe Introducir El Codigo de la Imagen",
        ];
    }  
}
