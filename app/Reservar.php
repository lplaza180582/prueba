<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Reservar extends Model
{

	use SoftDeletes;

    protected $table = 'reservas';
    protected $fillable = [
    //horasoli,codpersona,codconcepto
    'id',
    'nombre',
    'idrest',
    'fecha'
	];

    //protected $dates = ['deleted_at'];
    //
}
   